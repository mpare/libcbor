# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/nico/Software/libcbor/test/assertions.c" "/Users/nico/Software/libcbor/test/CMakeFiles/pretty_printer_test.dir/assertions.c.o"
  "/Users/nico/Software/libcbor/test/pretty_printer_test.c" "/Users/nico/Software/libcbor/test/CMakeFiles/pretty_printer_test.dir/pretty_printer_test.c.o"
  "/Users/nico/Software/libcbor/test/stream_expectations.c" "/Users/nico/Software/libcbor/test/CMakeFiles/pretty_printer_test.dir/stream_expectations.c.o"
  )
set(CMAKE_C_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "CBOR_BUFFER_GROWTH=2"
  "PRETTY_PRINTER=1"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/nico/Software/libcbor/src/CMakeFiles/cbor.dir/DependInfo.cmake"
  )
