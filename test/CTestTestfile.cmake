# CMake generated Testfile for 
# Source directory: /Users/nico/Software/libcbor/test
# Build directory: /Users/nico/Software/libcbor/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(bad_inputs_test "bad_inputs_test")
add_test(callbacks_test "callbacks_test")
add_test(cbor_serialize_test "cbor_serialize_test")
add_test(cbor_stream_decode_test "cbor_stream_decode_test")
add_test(copy_test "copy_test")
add_test(fuzz_test "fuzz_test")
add_test(pretty_printer_test "pretty_printer_test")
add_test(type_0_encoders_test "type_0_encoders_test")
add_test(type_0_test "type_0_test")
add_test(type_1_encoders_test "type_1_encoders_test")
add_test(type_1_test "type_1_test")
add_test(type_2_encoders_test "type_2_encoders_test")
add_test(type_2_test "type_2_test")
add_test(type_3_encoders_test "type_3_encoders_test")
add_test(type_3_test "type_3_test")
add_test(type_4_encoders_test "type_4_encoders_test")
add_test(type_4_test "type_4_test")
add_test(type_5_encoders_test "type_5_encoders_test")
add_test(type_5_test "type_5_test")
add_test(type_6_encoders_test "type_6_encoders_test")
add_test(type_6_test "type_6_test")
add_test(type_7_encoders_test "type_7_encoders_test")
add_test(type_7_test "type_7_test")
add_test(unicode_test "unicode_test")
