# CMake generated Testfile for 
# Source directory: /Users/nico/Software/libcbor
# Build directory: /Users/nico/Software/libcbor
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(src)
subdirs(test)
subdirs(examples)
